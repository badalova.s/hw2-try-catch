// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// При роботі з сервером, коли з якихось причин немає данних, користувач не дивиться в консоль і не розуміє помилок, тому за допомогую try..catch, можно обробити 
// помилку, повідомити про випадок користувачу, та та виконати необхідні дії для корректного завершення роботи
const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
 
 
  let root=document.getElementById("root")
  let vals=[]


function createValue (arr){
  arr.forEach(el=>{Object.entries(el).map(([key, value]) => {
    vals.push(`${key}`)
  });})
return vals= [...new Set(vals)]}
createValue (books)



function createList (arr){
  let ul =document.createElement('ul')
  root.appendChild(ul)
      arr.map(el=> {
      vals.map(e => { 
        if (el[e]) {
          let li =document.createElement('li')
          li.textContent=el[e]
          ul.appendChild(li)
      }})})
      
    }
 createList (books)

  function valid(arr){
      arr.map(el=> {
      vals.map(e => { 
    try {
      if (!el[e]) {           
        throw new Error (`Відсутня дані ${e} у книзі ${el.name}`)
    }
      
  }
    catch (error) {
    console.log(error.message)
      
   } 
  })})
  }

  valid(books)
 